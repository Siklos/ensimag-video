#include <pthread.h>
#include "synchro.h"
#include "ensitheora.h"


bool fini;

/* les variables pour la synchro, ici */
pthread_cond_t cond_window_create, cond_texture_ready, cond_cons, cond_deposer;
pthread_mutex_t mutex_taille_window;
pthread_mutex_t mutex_window;
pthread_mutex_t mutex_texture;
pthread_mutex_t mutex_window_texture;
pthread_mutex_t mutex_depot_consom;
bool texture_fini = 0;
int nbTextures = 0;

/* l'implantation des fonctions de synchro ici */
void envoiTailleFenetre(th_ycbcr_buffer buffer) {
	// deep copy
    pthread_mutex_lock(&mutex_taille_window);
    windowsx =  buffer[0].width;
	windowsy = buffer[0].height;
    pthread_cond_signal(&cond_window_create);
    pthread_mutex_unlock(&mutex_taille_window);
}

void attendreTailleFenetre() {
	pthread_mutex_lock(&mutex_taille_window);
	while (windowsx == 0 && windowsy == 0) {
		pthread_cond_wait(&cond_window_create, &mutex_taille_window);
	}
	pthread_mutex_unlock(&mutex_taille_window);
}

void signalerFenetreEtTexturePrete() {
	texture_fini = 1;
    pthread_mutex_lock(&mutex_window_texture);
    pthread_cond_signal(&cond_texture_ready);
    pthread_mutex_unlock(&mutex_window_texture);
}

void attendreFenetreTexture() {
	pthread_mutex_lock(&mutex_window_texture);
    while (!texture_fini) {
        pthread_cond_wait(&cond_texture_ready, &mutex_window_texture);
    }
    pthread_mutex_unlock(&mutex_window_texture);
}

void debutConsommerTexture() {
    pthread_mutex_lock(&mutex_depot_consom);
    while(nbTextures == 0) {
        pthread_cond_wait(&cond_cons, &mutex_depot_consom);
    }
    pthread_mutex_unlock(&mutex_depot_consom);

}

void finConsommerTexture() {
	nbTextures--;
    pthread_mutex_lock(&mutex_depot_consom);
    // if (nbTextures == NBTEX - 1)
    pthread_cond_signal(&cond_deposer);
    pthread_mutex_unlock(&mutex_depot_consom);
}


void debutDeposerTexture() {
    pthread_mutex_lock(&mutex_depot_consom);
    while(nbTextures == NBTEX) {
        pthread_cond_wait(&cond_deposer, &mutex_depot_consom);
    }
    pthread_mutex_unlock(&mutex_depot_consom);
}

void finDeposerTexture() {
	nbTextures++;
    pthread_mutex_lock(&mutex_depot_consom);
    // if (nbTextures == 1)
    pthread_cond_signal(&cond_cons);
    pthread_mutex_unlock(&mutex_depot_consom);
}
